'use strict'


document.addEventListener('DOMContentLoaded', function() {                                  // часть скрипта загружается только после загрузки ДОМа

    ymaps.ready(init)                                                                           // объявление init
    // document.getElementById("mainBlock").onclick = (event) => {                                 // событие на блок списка точек
    //     let target = event.target.parentElement
    //     let testArr =  Array.from(target.parentElement.children)
    //     polyline.geometry.remove(testArr.indexOf(target))
    //     target.remove()
    // }
})



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function enterThis() {                                                                      // объявление функции создания Метки при вводе имени и нажатии Enter
    
    let mainInput = document.querySelector('.main-creatingRoute__inputTxt input')                // объявление поля ввода
    let elemList = document.querySelector('.main-creatingRoute__pointList')                      // объявление блока, содержащего блоки с именами меток

    let charCount = Array.from(mainInput.value).length
    console.log(charCount)

    let newCheckBox = document.createElement('button')                                      // создание кнопки для удаления блока Метки (и самой метки)



    let placemark = new ymaps.Placemark(map.getCenter(), {
        balloonContentHeader: mainInput.value
    },                                // 
    {
        iconLayout: 'default#image',  
        iconImageHref: '../sources/images/point.png',  
        iconImageSize: [26, 26],  
        iconImageOffset: [-12, -30], 
        draggable: true                                                                         // перетаскиваемая метка
    })

    if (charCount > 0 && charCount <= 8) {
        polyline.geometry.insert(polyline.geometry.getLength(), map.getCenter())
        let getLengthTest = polyline.geometry.getLength() - 1
        // polyline.editor.startEditing()



        map.geoObjects.add(placemark)

        placemark.events.add('dragend', function (e) {                                          //событие перетаскивания метки
            let target = e.get('target').geometry.getCoordinates()                                      //объявление target, хранящей координаты метки
            // console.log(target)
            polyline.geometry.getCoordinates()[getLengthTest] = target
            polyline.geometry.insert(polyline.geometry.getLength(), map.getCenter())            // не понимаю как это заработало
            polyline.geometry.remove(polyline.geometry.getLength() - 1)
        });
    
    
        let newPoint = document.createElement('div')                                            // создание блока Метки в elemList
        newPoint.classList.add('main-creatingRoute__pointList-elem')                                     // добавление класса блоку Метки
        let str = mainInput.value
        str = str.replaceAll(' ', '_')
        newPoint.innerHTML = str                                                                // помещение в блок Метки ее название
        
        newCheckBox.classList.add('main-creatingRoute__pointList-elem-checkbox')                         // добавление класса кнопке удаления
        
    
        newPoint.insertAdjacentElement('beforeEnd', newCheckBox)                                // при создании, кнопка удаления вставляется в конец содержимого блока Метки
        elemList.insertAdjacentElement('beforeEnd', newPoint)                                   // при создании, блок Метки вставляется в конец содержимого блока, хранящего блоки Меток
        mainInput.value = ""                                                                    // удаление из поля ввода названия созданной метки
    } else if(charCount <= 0) { 
        mainInput.value = ""
        alert("Ошибка! Нельзя оставлять поле ввода пустым.")
    } else if(charCount > 8) {
        mainInput.value = ""
        alert("Ошибка! Нельзя вводить больше 8 символов.")
    }



    newCheckBox.onclick = (event) => {
        map.geoObjects.remove(placemark)
        let target = event.target.parentElement
        let testArr =  Array.from(target.parentElement.children)
        polyline.geometry.remove(testArr.indexOf(target))
        target.remove()
    }




    return false                                                                            // не отправлять форму и не обнавлять страницу при нажатии Enter
}







let map                                                                                     // объявление карты
let polyline                                                                                // объявление пути

function init() {
    map = new ymaps.Map('map',                                                              // создание карты
                        {
                            center: [58.02080072074206,38.84380018387735],
                            zoom: 17
                        })

    polyline = new ymaps.Polyline([], { hintContent: "Polyline" },                          // объявление пути, его параметров и опций
                                {
                                    strokeColor: '#00BD9F',
                                    strokeWidth: 4,
                                    strokeStyle: '1 3',
                                    draggable: true,
                                    dragViaPoints: false
                                })

    map.geoObjects.add(polyline)                                                            // создание геообъекта polyline (пути)
}